import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import Albums from "./src/components/Albums/Albums";
import Photos from "./src/components/Photos/Photos";
import PhotoDetails from "./src/components/Photos/PhotoDetails";
import { Provider } from "./src/context/Provider";
import Notification from "./src/components/generics/Notification";
import { Appbar, Avatar } from "react-native-paper";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import Profile from "./src/components/MyProfile/Profile";
import Settings from "./src/components/Settings/Settings";
import TraductorText from "./src/utils/TraductorText";

const Stack = createStackNavigator();
const Tab = createMaterialBottomTabNavigator();

const Header = ({ scene, previous, navigation }) => {
  const { options } = scene.descriptor;
  return (
    <Appbar.Header>
      {previous ? (
        <Appbar.BackAction onPress={() => navigation.goBack()} />
      ) : null}
      <Appbar.Content title={options.title} />
    </Appbar.Header>
  );
};

function AlbumsStackNavigator() {
  return (
    <Stack.Navigator
      screenOptions={{
        header: ({ scene, previous, navigation }) => (
          <Header scene={scene} previous={previous} navigation={navigation} />
        ),
      }}
    >
      <Stack.Screen
        name="albumList"
        component={Albums}
        options={{ title: <TraductorText id="albums"></TraductorText> }}
      />
      <Stack.Screen
        name="photosList"
        component={Photos}
        options={{ title: <TraductorText id="photos"></TraductorText> }}
      />
      <Stack.Screen
        name="photoDetails"
        component={PhotoDetails}
        options={{ title: <TraductorText id="photoDetails"></TraductorText> }}
      />
    </Stack.Navigator>
  );
}

export default function App() {
  return (
    <Provider>
      <NavigationContainer>
        <Tab.Navigator barStyle={{ backgroundColor: "#6200ee" }}>
          <Tab.Screen
            name="AlbumsStackNavigator"
            component={AlbumsStackNavigator}
            options={{
              tabBarLabel: <TraductorText id="albums"></TraductorText>,
              tabBarIcon: ({ color }) => (
                <MaterialCommunityIcons
                  name="image-album"
                  color={color}
                  size={26}
                />
              ),
            }}
          />
          <Tab.Screen
            name="MyProfile"
            component={Profile}
            options={{
              tabBarLabel: <TraductorText id="myProfile"></TraductorText>,
              tabBarIcon: ({ color }) => (
                <MaterialCommunityIcons
                  name="account-circle"
                  color={color}
                  size={26}
                />
              ),
            }}
          />
          <Tab.Screen
            name="Config"
            component={Settings}
            options={{
              tabBarLabel: <TraductorText id="setting"></TraductorText>,
              tabBarIcon: ({ color }) => (
                <MaterialCommunityIcons
                  name="settings"
                  color={color}
                  size={26}
                />
              ),
            }}
          />
        </Tab.Navigator>
        <Notification></Notification>
      </NavigationContainer>
    </Provider>
  );
}
