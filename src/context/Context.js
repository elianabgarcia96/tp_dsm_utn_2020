import { createContext } from "react";
import languages from "../languages/languages";
import dictionaryList from "../languages/dictionaryList";

export const Context = createContext({
  error: { active: false, message: "" },
  theme: "light",
  selectedLanguage: languages[0],
  languageOptions: languages,
  dictionary: dictionaryList[languages[0].code],
});
