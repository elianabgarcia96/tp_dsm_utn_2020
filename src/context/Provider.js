import React, { useContext, useState } from "react";
import { Context } from "./Context";
import dictionaryList from "../languages/dictionaryList";

export function Provider(props) {
  const context = useContext(Context);
  const [error, setError] = useState(context.error);
  const [selectedLanguage, setselectedLanguage] = useState(
    context.selectedLanguage
  );
  const [dictionary, setDictionary] = useState(context.dictionary);

  const provider = {
    error,
    selectedLanguage,
    dictionary,
    setError: (bool, message) => {
      setError({ active: bool, message });
      setTimeout(() => {
        setError({ active: false, message: "" });
      }, 8000);
    },
    changeLanguage: (selectedLanguage) => {
      setselectedLanguage(selectedLanguage);
      setDictionary(dictionaryList[selectedLanguage.code]);
    },
  };

  return <Context.Provider value={provider}>{props.children}</Context.Provider>;
}
