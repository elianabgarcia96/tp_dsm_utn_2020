import EN from "./EN";
import ES from "./ES";

let dictionaryList = {
  ES,
  EN,
};

export default dictionaryList;
