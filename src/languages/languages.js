import ESflag from "../../assets/flags/ES.png";
import ENflag from "../../assets/flags/EN.png";

let languages = [
  { code: "ES", name: "Español", flag: ESflag },
  { code: "EN", name: "English", flag: ENflag },
  // { code: "IT", name: "Italiano" },
  // { code: "PT", name: "Português" },
];

export default languages;
