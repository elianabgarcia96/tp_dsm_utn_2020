import React, { useEffect, useState, useContext } from "react";
import { View, StyleSheet, ScrollView } from "react-native";
import Album from "./Album";
import { getAlbumsUrl } from "../../utils/urls";
import axios from "axios";
import { Context } from "../../context/Context";
import Loading from "../generics/Loading";
import Empty from "../generics/Empty";
import TraductorText from "../../utils/TraductorText";

const Albums = (props) => {
  let { navigation } = props;
  const [albums, setalbums] = useState(null);

  const context = useContext(Context);

  useEffect(() => {
    const getAlbums = async () => {
      await axios
        .get(getAlbumsUrl)
        .then((response) => {
          setalbums(response.data.photosets.photoset);
        })
        .catch((e) => {
          context.setError(
            true,
            <TraductorText id="errorGetAlbums"></TraductorText>
          );
          setalbums([]);
        });
    };
    getAlbums();
  }, []);

  if (!albums) {
    return <Loading></Loading>;
  } else if (albums.length > 0) {
    return (
      <ScrollView style={styles.scrollview}>
        <View style={styles.container}>
          {albums.map((a, key) => (
            <Album
              key={a.id}
              album={a}
              selectAlbum={() =>
                navigation.navigate("photosList", { albumId: a.id })
              }
            ></Album>
          ))}
        </View>
      </ScrollView>
    );
  }
  return (
    <Empty text={<TraductorText id="emptyUserAlbums"></TraductorText>}></Empty>
  );
};

const styles = StyleSheet.create({
  scrollview: {
    flex: 1,
  },

  container: {
    padding: 30,
    flexWrap: "nowrap",
  },
});

export default Albums;
