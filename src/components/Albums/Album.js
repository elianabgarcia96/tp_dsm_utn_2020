import React from "react";
import {
  View,
  Text,
  Image,
  StyleSheet,
  Dimensions,
  ImageBackground,
} from "react-native";
import { Avatar, Button, Card, Title, Paragraph } from "react-native-paper";
import { Ionicons, FontAwesome } from "@expo/vector-icons";
import { TouchableOpacity } from "react-native-gesture-handler";

const Album = (props) => {
  let { album, selectAlbum } = props;

  return (
    <TouchableOpacity onPress={selectAlbum} key={album.id}>
      <ImageBackground
        source={{ uri: `https://unsplash.it/300/300/?random&__id=${album.id}` }}
        style={{
          flex: 1,
          justifyContent: "flex-end",
          height: Dimensions.get("window").width / 1.6,
          width: "100%",
          marginBottom: 30,
        }}
        imageStyle={{ borderRadius: 25 }}
      >
        <View
          style={{
            backgroundColor: "#ddd7d7a8",
            borderBottomEndRadius: 25,
            borderBottomStartRadius: 25,
            flex: 0.3,
            padding: 10,
            paddingHorizontal: 15,
          }}
        >
          <Text style={{ color: "#000", fontSize: 20, fontWeight: "bold" }}>
            {album.title._content}
          </Text>
          <View
            style={{
              flexDirection: "row",
              marginTop: 8,
              alignItems: "center",
            }}
          >
            <Text
              style={{
                fontSize: 16,
                marginRight: 20,
              }}
            >
              <Ionicons name="md-photos" size={20} /> {album.count_photos}
            </Text>
            <Text style={{ fontSize: 16 }}>
              <Ionicons
                name="ios-eye"
                size={22}
                iconStyle={{ marginRight: 50 }}
              />{" "}
              {album.count_views}
            </Text>
          </View>
        </View>
      </ImageBackground>
    </TouchableOpacity>
  );
};

export default Album;
