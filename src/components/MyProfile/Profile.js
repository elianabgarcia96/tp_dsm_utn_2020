import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { Avatar, Appbar, Button } from "react-native-paper";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import TraductorText from "../../utils/TraductorText";

let info = [
  {
    data: <TraductorText id="studentID"></TraductorText>,
    value: 70964,
  },
  {
    data: <TraductorText id="email"></TraductorText>,
    value: "elianabgarcia@gmail.com",
  },
  {
    data: <TraductorText id="birthdate"></TraductorText>,
    value: "18/09/1996",
  },
];

export default function Profile(props) {
  let { navigation } = props;
  return (
    <View style={styles.container}>
      <Appbar.Header>
        <Appbar.Content
          title={<TraductorText id="myProfile"></TraductorText>}
        />
      </Appbar.Header>
      <View style={styles.content}>
        <Avatar.Image
          size={100}
          source={require("../../../assets/user.jpeg")}
        />
        <Text style={styles.name}>Eliana García</Text>
        <Text style={styles.university}>UTN - FRC</Text>
        <View style={styles.infoContainer}>
          {info.map((i, key) => (
            <View key={key} style={styles.row}>
              <Text style={styles.infoData}>{i.data}:</Text>
              <Text style={styles.infoValue}>{i.value}</Text>
            </View>
          ))}
        </View>
        <Button
          mode="contained"
          icon="folder-multiple-image"
          onPress={() => navigation.navigate("albumList")}
          style={styles.button}
        >
          <TraductorText id="myAlbums"></TraductorText>
        </Button>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    alignItems: "center",
    paddingTop: 40,
    flex: 1,
  },
  name: {
    fontSize: 28,
    fontWeight: "bold",
  },
  university: {
    fontSize: 14,
  },
  infoContainer: {
    width: "80%",
    marginTop: 40,
  },
  row: {
    flexDirection: "row",
    justifyContent: "space-between",
    backgroundColor: "#fff",
    paddingVertical: 10,
    paddingHorizontal: 30,
    borderRadius: 40,
    shadowColor: "#000",
    shadowOpacity: 0.1,
    shadowRadius: 6,
    elevation: 5,
    marginBottom: 30,
  },
  infoData: {
    color: "#b3b3b3",
    fontSize: 15,
  },
  infoValue: {
    fontWeight: "bold",
    fontSize: 15,
  },
  button: {
    borderRadius: 40,
    paddingHorizontal: 8,
    paddingVertical: 3,
  },
});
