import React, { useState, useContext } from "react";
import { ScrollView, View, StyleSheet, Image } from "react-native";
import { Button, Dialog, List, Divider } from "react-native-paper";
import ESflag from "../../../assets/flags/ES.png";
import ENflag from "../../../assets/flags/EN.png";
import { Context } from "../../context/Context";
import languages from "../../languages/languages";

export default function ChangeLanguage(props) {
  let { visible, close } = props;
  const [checked, setchecked] = useState(languages[0]);

  const context = useContext(Context);

  return (
    <Dialog visible={visible}>
      <Dialog.Title>Elegir lenguaje</Dialog.Title>
      <Divider></Divider>
      <Dialog.ScrollArea style={{ maxHeight: 170, paddingHorizontal: 0 }}>
        <ScrollView>
          <View style={{ margin: 18 }}>
            {languages.map((o, k) => {
              return (
                <List.Item
                  key={o.code}
                  onPress={() => setchecked(o)}
                  style={
                    checked.code === o.code
                      ? { backgroundColor: "#d0d2d7" }
                      : {}
                  }
                  title={o.name}
                  left={(props) => (
                    <Image
                      style={styles.flag}
                      height={20}
                      width={30}
                      source={o.flag}
                    />
                  )}
                />
              );
            })}
          </View>
        </ScrollView>
      </Dialog.ScrollArea>
      <Dialog.Actions>
        <Button
          onPress={() => {
            context.changeLanguage(checked);
            close();
          }}
        >
          Aceptar
        </Button>
      </Dialog.Actions>
    </Dialog>
  );
}

const styles = StyleSheet.create({
  row: {
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  text: {
    paddingLeft: 8,
  },
  flag: {
    marginTop: 8,
  },
});
