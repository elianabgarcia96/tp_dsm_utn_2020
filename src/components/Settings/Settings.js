import React, { useState } from "react";
import { View, StyleSheet } from "react-native";
import { List, Appbar, Divider, Avatar } from "react-native-paper";
import ChangeLanguage from "./ChangeLanguage";
import TraductorText from "../../utils/TraductorText";

export default function Settings() {
  const [changeLanguage, setchangeLanguage] = useState(false);
  return (
    <View style={styles.container}>
      <Appbar.Header>
        <Appbar.Content title={<TraductorText id="setting"></TraductorText>} />
      </Appbar.Header>
      <List.Section style={styles.list}>
        <List.Item
          onPress={() => setchangeLanguage(true)}
          style={styles.item}
          left={() => (
            <Avatar.Icon
              size={38}
              icon="comment-text-multiple-outline"
              style={styles.icon}
            />
          )}
          title={<TraductorText id="language"></TraductorText>}
          description={
            <TraductorText id="changeTheAppLanguage"></TraductorText>
          }
        />
        <Divider></Divider>
        <List.Item
          style={styles.item}
          left={() => (
            <Avatar.Icon size={38} icon="account-circle" style={styles.icon} />
          )}
          title={<TraductorText id="myAccount"></TraductorText>}
        />
        <Divider></Divider>
        <List.Item
          style={styles.item}
          left={() => <Avatar.Icon size={38} icon="bell" style={styles.icon} />}
          title={<TraductorText id="notifications"></TraductorText>}
        />
        <Divider></Divider>
        <List.Item
          style={styles.item}
          left={() => (
            <Avatar.Icon
              size={38}
              icon="shield-lock-outline"
              style={styles.icon}
            />
          )}
          title={<TraductorText id="privacyAndSecurity"></TraductorText>}
        />
        <Divider></Divider>
        <List.Item
          style={styles.item}
          left={() => (
            <Avatar.Icon
              size={38}
              icon="arrow-right-bold-outline"
              style={styles.icon}
            />
          )}
          title={<TraductorText id="logout"></TraductorText>}
        />
        <Divider></Divider>
      </List.Section>
      <ChangeLanguage
        visible={changeLanguage}
        close={() => setchangeLanguage(false)}
      ></ChangeLanguage>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  icon: {
    marginTop: 8,
    marginRight: 10,
  },
  list: {
    paddingHorizontal: 15,
  },
  item: {
    marginVertical: 8,
  },
});
