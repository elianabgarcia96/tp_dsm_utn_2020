import React, { useContext } from "react";
import { Snackbar } from "react-native-paper";
import { Context } from "../../context/Context";

export default function (props) {
  const context = useContext(Context);
  return (
    <Snackbar
      visible={context.error.active}
      style={{ backgroundColor: "#b0342f" }}
      duration={1000}
    >
      {context.error.message}
    </Snackbar>
  );
}
