import React from "react";
import { View, Text, Image, StyleSheet } from "react-native";
import image from "../../../assets/notFound.png";

export default function Empty(props) {
  let { text } = props;
  return (
    <View style={styles.container}>
      <Image source={image} style={styles.image}></Image>
      <Text style={styles.text}>{text}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    justifyContent: "space-evenly",
    paddingVertical: 70,
    alignItems: "center",
  },
  image: {
    width: "80%",
    height: "50%",
  },
  text: {
    fontSize: 30,
    fontWeight: "bold",
    color: "#20232a",
    textAlign: "center",
    paddingHorizontal: 38,
  },
});
