import React from "react";
import { ActivityIndicator } from "react-native-paper";
import { View } from "react-native";

const Loading = () => {
  return (
    <View
      style={{
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <ActivityIndicator size="large" animating={true} />
    </View>
  );
};

export default Loading;
