import React from "react";
import { View, Text, StyleSheet, Dimensions, Image } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";

const Photo = (props) => {
  let { photo, selectPhoto, uri, key } = props;

  return (
    <View
      style={{
        height: Dimensions.get("window").width / 2,
        width: "50%",
      }}
    >
      <TouchableOpacity onPress={selectPhoto}>
        <View style={styles.item}>
          <Image source={{ uri }} style={styles.photo} />
          <Text style={{ marginTop: 10, fontSize: 15, fontWeight: "bold" }}>
            {photo.title}
          </Text>
          <Text style={{ fontSize: 12 }}>
            {/* {photo.datetaken} */}
            {photo.datetaken.substring(0, 10)}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  item: {
    height: Dimensions.get("window").width / 2,
    width: "100%",
    padding: 8,
  },

  photo: {
    flex: 1,
    resizeMode: "cover",
    borderRadius: 12,
  },
});

export default Photo;
