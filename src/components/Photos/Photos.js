import React, { useEffect, useState, useContext } from "react";
import { View, ScrollView, StyleSheet, Dimensions, Text } from "react-native";
import { getAlbumPhotosUrl, getPhotoUrl } from "../../utils/urls";
import { Button, ToggleButton } from "react-native-paper";

import axios from "axios";
import Photo from "./Photo";
import Loading from "../generics/Loading";
import Empty from "../generics/Empty";
import { Context } from "../../context/Context";
import { AntDesign } from "@expo/vector-icons";
import SortPhotos from "./SortPhotos";
import TraductorText from "../../utils/TraductorText";

const Photos = (props) => {
  let { navigation } = props;
  const [photos, setphotos] = useState(null);
  const [openSortModal, setopenSortModal] = useState(false);
  const [orderBy, setorderBy] = useState(null);

  const context = useContext(Context);

  useEffect(() => {
    const getPhotos = async () => {
      let { albumId } = props.route.params;
      await axios
        .get(getAlbumPhotosUrl(albumId))
        .then((response) => {
          setphotos(response.data.photoset.photo);
        })
        .catch((e) => {
          context.setError(
            true,
            <TraductorText id="errorGetPhotos"></TraductorText>
          );
          setphotos([]);
        });
    };
    getPhotos();
  }, []);

  if (!photos) {
    return <Loading></Loading>;
  } else if (photos.length > 0) {
    return (
      <View style={{ flex: 1, alignItems: "center" }}>
        <ScrollView>
          <View style={styles.row}>
            <Button mode="outlined" onPress={() => setopenSortModal(true)}>
              {orderBy ? orderBy : <TraductorText id="orderBy"></TraductorText>}
              <AntDesign name="caretdown" size={13} color="#6200ee" />
            </Button>
          </View>
          <View style={styles.grid}>
            {photos.map((p, k) => (
              <Photo
                key={k}
                photo={p}
                uri={getPhotoUrl(p.farm, p.server, p.secret, p.id)}
                selectPhoto={() =>
                  navigation.navigate("photoDetails", {
                    photoId: p.id,
                    uri: getPhotoUrl(p.farm, p.server, p.secret, p.id),
                  })
                }
              ></Photo>
            ))}
          </View>
        </ScrollView>
        <SortPhotos
          visible={openSortModal}
          close={() => setopenSortModal(false)}
          photos={photos}
          setorderBy={(string) => setorderBy(string)}
        ></SortPhotos>
      </View>
    );
  }
  return (
    <Empty text={<TraductorText id="emptyPhotosAlbum"></TraductorText>}></Empty>
  );
};

const styles = StyleSheet.create({
  grid: {
    flexDirection: "row",
    flexWrap: "wrap",
    padding: 10,
  },
  item: {
    height: Dimensions.get("window").width / 2,
    width: "50%",
    padding: 8,
  },

  photo: {
    flex: 1,
    resizeMode: "cover",
    borderRadius: 12,
  },
  row: {
    flexDirection: "row-reverse",
    flexWrap: "wrap",
    paddingHorizontal: 18,
    paddingTop: 20,
  },
});

export default Photos;
