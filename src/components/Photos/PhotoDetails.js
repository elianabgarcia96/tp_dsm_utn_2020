import React, { useEffect, useState } from "react";
import {
  View,
  Linking,
  Image,
  Text,
  StyleSheet,
  ScrollView,
} from "react-native";
import axios from "axios";
import { getCommentsUrl } from "../../utils/urls";
import { ToggleButton } from "react-native-paper";
import { Ionicons, MaterialCommunityIcons } from "@expo/vector-icons";
import Loading from "../generics/Loading";
import Comments from "./Comments";

const PhotoDetails = (props) => {
  let { photoId, uri } = props.route.params;
  const [comments, setcomments] = useState(null);
  const [liked, setliked] = useState(Math.random() >= 0.5);
  const [randomLikes, setrandomLikes] = useState(
    5 + Math.floor((999 - 5) * Math.random())
  );

  useEffect(() => {
    async function getComments() {
      await axios.get(getCommentsUrl(photoId)).then((response) => {
        setcomments(response.data.comments.comment);
      });
    }
    getComments();
  }, []);

  if (!comments) {
    return <Loading></Loading>;
  }
  return (
    <View style={styles.item}>
      <View
        style={{
          flex: 1,
          backgroundColor: "#fff",
          borderRadius: 20,
          padding: 5,
        }}
      >
        <Image source={{ uri }} style={styles.photo} />

        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
            marginRight: 10,
          }}
        >
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <ToggleButton
              icon={liked ? "heart" : "heart-outline"}
              color={liked ? "red" : "black"}
              value="bluetooth"
              onPress={() => setliked(!liked)}
              size={30}
            />
            <Text style={{ fontWeight: "700", fontSize: 20, marginRight: 20 }}>
              {liked ? randomLikes + 1 : randomLikes}
            </Text>
            <MaterialCommunityIcons
              name="chat-outline"
              size={28}
              color="black"
            />
            <Text style={{ fontWeight: "700", fontSize: 20, marginLeft: 5 }}>
              {comments.length}
            </Text>
          </View>

          <Ionicons
            name="ios-share-alt"
            size={32}
            color="black"
            onPress={() => Linking.openURL(uri)}
          />
        </View>
      </View>
      <ScrollView style={{ flex: 1 }}>
        {comments.map((c, key) => (
          <Comments key={key} comment={c}></Comments>
        ))}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  item: {
    width: "100%",
    padding: 10,
    flex: 1,
  },

  photo: {
    flex: 1,
    resizeMode: "cover",
    borderRadius: 12,
    marginBottom: 20,
    marginTop: 10,
    marginHorizontal: 8,
  },
});

export default PhotoDetails;
