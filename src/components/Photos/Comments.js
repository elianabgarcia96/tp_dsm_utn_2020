import React from "react";
import { List, Avatar, Divider, ToggleButton } from "react-native-paper";
import { getUserAvatarUrl } from "../../utils/urls";

export default function Comments(props) {
  let { comment } = props;
  return (
    <>
      <List.Item
        key={comment.id}
        title={comment.authorname}
        description={comment._content}
        descriptionNumberOfLines={4}
        style={{ alignItems: "center" }}
        left={(props) => (
          <Avatar.Image
            size={42}
            style={{ marginTop: 8, marginRight: 5 }}
            source={{
              uri: getUserAvatarUrl(
                comment.iconfarm,
                comment.iconserver,
                comment.author
              ),
            }}
          />
        )}
      />
      <Divider />
    </>
  );
}
