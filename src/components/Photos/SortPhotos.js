import React from "react";
import { ScrollView, View, StyleSheet } from "react-native";
import {
  Subheading,
  Button,
  Dialog,
  RadioButton,
  TouchableRipple,
} from "react-native-paper";
import { AntDesign } from "@expo/vector-icons";
import TraductorText from "../../utils/TraductorText";

let options = [
  { name: "nameD", desc: <TraductorText id="nameA"></TraductorText> },
  { name: "nameA", desc: <TraductorText id="nameD"></TraductorText> },

  { name: "dateD", desc: <TraductorText id="createdDateA"></TraductorText> },
  { name: "dateA", desc: <TraductorText id="createdDateD"></TraductorText> },
];

export default function SortPhotos(props) {
  let { photos, visible, close, setorderBy } = props;
  const [checked, setChecked] = React.useState("");

  const sort = () => {
    let array = photos;
    switch (checked.name) {
      case "nameD":
        array.sort(function (a, b) {
          if (a.title > b.title) return 1;
          if (a.title < b.title) return -1;
          return 0;
        });
        break;
      case "nameA":
        array.sort(function (a, b) {
          if (a.title < b.title) return 1;
          if (a.title > b.title) return -1;
          return 0;
        });
        break;
      case "dateD":
        array.sort(function (a, b) {
          if (a.datetaken > b.datetaken) return 1;
          if (a.datetaken < b.datetaken) return -1;
          return 0;
        });
        break;
      case "dateA":
        array.sort(function (a, b) {
          if (a.datetaken < b.datetaken) return 1;
          if (a.datetaken > b.datetaken) return -1;
          return 0;
        });
        break;
      default:
        break;
    }
    setorderBy(checked.desc);

    close();
  };
  return (
    <Dialog visible={visible}>
      <Dialog.Title>
        <TraductorText id="sortPhotos"></TraductorText>
      </Dialog.Title>
      <Dialog.ScrollArea style={{ maxHeight: 170, paddingHorizontal: 0 }}>
        <ScrollView>
          <View>
            {options.map((o, k) => (
              <TouchableRipple onPress={() => setChecked(o)} key={k}>
                <View style={styles.row}>
                  <View pointerEvents="none">
                    <RadioButton
                      value="normal"
                      status={checked.name === o.name ? "checked" : "unchecked"}
                    />
                  </View>
                  <Subheading style={styles.text}>
                    {o.desc}
                    <AntDesign
                      name={o.name.slice(-1) === "D" ? "arrowdown" : "arrowup"}
                      size={16}
                      color="black"
                    />
                  </Subheading>
                </View>
              </TouchableRipple>
            ))}
          </View>
        </ScrollView>
      </Dialog.ScrollArea>
      <Dialog.Actions>
        <Button onPress={() => sort()}>
          <TraductorText id="accept"></TraductorText>
        </Button>
      </Dialog.Actions>
    </Dialog>
  );
}

const styles = StyleSheet.create({
  row: {
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  text: {
    paddingLeft: 8,
  },
});
