import { api_key, user_id } from "./settings";

export const getAlbumsUrl = `https://api.flickr.com/services/rest/?method=flickr.photosets.getList&api_key=${api_key}&format=json&nojsoncallback=1&user_id=${user_id}`;

export const getAlbumPhotosUrl = (albumId) =>
  `https://api.flickr.com/services/rest/?method=flickr.photosets.getPhotos&api_key=6e8a597cb502b7b95dbd46a46e25db8d&photoset_id=${albumId}&user_id=${user_id}&format=json&nojsoncallback=1&extras=date_taken`;

export const getPhotoUrl = (farm, server, secret, id) =>
  `https://farm${farm}.staticflickr.com/${server}/${id}_${secret}.jpg`;

export const getCommentsUrl = (idPhoto) =>
  `https://www.flickr.com/services/rest/?method=flickr.photos.comments.getList&api_key=${api_key}&photo_id=${idPhoto}&format=json&nojsoncallback=1`;

export const getUserAvatarUrl = (farm, server, userId) =>
  `https://farm${farm}.staticflickr.com/${server}/buddyicons/${userId}.jpg`;

export const getUser = (id) =>
  `https://www.flickr.com/services/rest/?method=flickr.people.getInfo&api_key=6e8a597cb502b7b95dbd46a46e25db8d&user_id=${id}&format=json&nojsoncallback=1`;
