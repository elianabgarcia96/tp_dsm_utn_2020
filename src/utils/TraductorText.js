import { useContext } from "react";
import { Context } from "../context/Context";

function TraductorText(props) {
  const context = useContext(Context);

  return context.dictionary[props.id];
}

export default TraductorText;
